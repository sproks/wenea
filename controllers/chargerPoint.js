'use strict'

var ChargerPoint = require('../models/chragerPoint');



function addChargePoint(req, res){
    var ChP = new ChargerPoint();
    var params = req.body;
    
    ChP.id = params.id;
    ChP.name = params.name;
    ChP.status = params.status;
    ChP.created_at = new Date();
    ChP.deleted_at = null;
    
    ChP.save((err, ChPStored)=>{
        console.log(ChP);
        if(err){
            res.status(500).send({message: 'error on save Charger to DB: '+err});
        }else{
            if(!ChPStored){
                res.status(404).send({message: 'Charger not stored to DB'});
            }else{
                res.status(200).send({message:'Charger added correctly',charger: ChPStored});

            }
        }
    });
}

function deleteChargePoint(req, res){
    var ChId = req.params.id;
    var dateDeleted = new Date();

    ChargerPoint.findOneAndUpdate({id:ChId}, {deleted_at: dateDeleted },(err,ChDeleted)=>{
        if(err){
            res.status(500).send({message: 'error on delete Charger to DB: '+err});
        }else{
            if(!ChDeleted){
                res.status(404).send({message: 'Charger not deleted DB'});
            }else{
                res.status(200).send({message:'Charger deleted correctly',charger: ChDeleted});
            }
        }
    });
}

function listChargers(req, res){
    ChargerPoint.find({deleted_at : null},(err,chargers)=>{
        if(err){
            res.status(500).send({message: 'error on get chargers: '+err});
        }else{
            if(!chargers){
                res.status(404).send({message: 'There are not chargers'});
            }else{
                res.status(200).send({chargers: chargers});
            }
        }
    });
}

function getCharger(req, res){
    var ChId = req.params.id;

    ChargerPoint.findOne({id: ChId}, (err, charger)=>{
        if(err){
            res.status(500).send({message: 'error on get charger: '+err});
        }else{
            if(!charger){
                res.status(404).send({message: 'Charger not found'});
            }else{
                res.status(200).send({charger: charger});
            }
        }
    });
}

function changeStatusCharger(req, res){
    var ChId = req.body.id;
    var status = req.body.status;

    ChargerPoint.findOneAndUpdate({id: ChId},{status: status},{runValidators: true}, (err, charger)=>{
        if(err){
            res.status(500).send({message: 'error update status: '+err});
        }else{
            if(!charger){
                res.status(404).send({message: 'Charger not found'});
            }else{
                res.status(200).send({message:'Status updated to: '+status ,charger: charger});
            }
        }
    });
}

module.exports = {
    addChargePoint,
    deleteChargePoint,
    listChargers,
    getCharger,
    changeStatusCharger
}