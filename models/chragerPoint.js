'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var ChargerPointSchema = Schema({
    id: Number,
    name:{ type: String, maxlength: 32 },
    status:{ type: String, enum:['ready', 'charging','waiting','error']},
    created_at: {type: Date, default: Date.now},
    deleted_at: Date
});


module.exports = mongoose.model('chargepoints', ChargerPointSchema);

