'use strict'

var express = require('express');
var ChragerPointController = require('../controllers/chargerPoint');

var api = express.Router();

api.post('/chargepoint',ChragerPointController.addChargePoint);
api.delete('/chargepoint/:id',ChragerPointController.deleteChargePoint);
api.get('/chargepoint',ChragerPointController.listChargers);
api.get('/chargepoint/:id',ChragerPointController.getCharger);
api.put('/chargepoint/status',ChragerPointController.changeStatusCharger);

module.exports = api;