'use strict'

var express = require('express');
var bodyParser = require('body-parser');



var app = express();


//set the template engine ejs

//loading rutes
var chargerPointRoutes = require('./routes/chragerPoint');

//convert requests to JSON
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

//middlewares
app.use(express.static( 'public'));

//base rutes
app.use('/api', chargerPointRoutes); //api routes (server)

//public 
app.get('/',(req, res)=>{
    res.render('index');
});

module.exports = app;